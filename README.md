# Lucid VPS Capacity Alert

### Requirements
You'll need file in `$HOME/.secrets` called `slack_sysadmin-webhook` which
defines the environment variable `SLACK_SYSADMIN_WEBHOOK_URL`.

It goes without saying you'll need a webhook url setup in slack for whatever
channel you're sending notifications to.

### Installation
Clone the repo, meet the above requirements, configure the THRESHOLD variable
in the script if you'd like it different than %90 capacity, and setup a cron
job to execute the script at your desired interval.


