#!/bin/bash
CURRENT=$(df / | grep / | awk '{ print $5}' | sed 's/%//g')
THRESHOLD=90

# Make sure we have our needed secrets
if [ ! -f "$HOME/.secrets/slack_sysadmin-webhook" ]; then
  exit 1
else
  source "$HOME/.secrets/slack_sysadmin-webhook"
fi

if [ "$CURRENT" -gt "$THRESHOLD" ] ; then
  response="$( date ) $HOSTNAME: Root partition remaining free space is critically low. Used: $CURRENT%"
  curl -X POST -H 'Content-type: application/json' --data '{"text":"'"$response"'"}' $SLACK_SYSADMIN_WEBHOOK_URL
fi
